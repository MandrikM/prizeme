import React, {Component} from 'react'

class Sixth extends Component {
    render() {
        return (
            <div id="sixth" className="Sixth">
                <form>
                    <div className="FourthText">
                    <label >

                        <input
                            name="isGoing"
                            type="checkbox"
                            checked="true"
                        />
                       {"Я, {{"+"Name}}, як Головний переможець Акції погоджуюсь отримати свій виграш – 317 000,00 гривень – готівкою."}
                    </label>
                    </div>

                    <p className="SixthLabel"> Я очікую доставку грошового Призу:</p>
                    <div className="App">
                        <div>
                            <input type="radio" id="contactChoice1"
                                    name="contact" value="email"/>
                            <label>Першого тижня виплат (01–05.05) </label>
                        </div>

                        <div>
                            <input type="radio" id="contactChoice2"
                                    name="contact" value="phone"/>
                            <label htmlFor="contactChoice2">Другого тижня виплат (06–12.05) </label>
                        </div>

                        <div>
                            <input type="radio" id="contactChoice3"
                                    name="contact" value="mail"/>
                            <label htmlFor="contactChoice3">Третього тижня виплат (13–19.05)</label>
                        </div>

                        <div>
                            <input type="radio" id="contactChoice3"
                                    name="contact" value="mail"/>
                            <label htmlFor="contactChoice3">Четвертого тижня виплат (20–26.05) </label>
                        </div>

                        <div>
                            <input type="radio" id="contactChoice3"
                                    name="contact" value="mail"/>
                            <label htmlFor="contactChoice3">П’ятого тижня виплат (27.05–31.05).</label>
                        </div>


                    </div>
                    <div className="App">
                        <label className="SixthLabel">
                            <div className="SixthText">В зручний для мене день тижня:</div>
                            <input type="text" value="Cубота" className="SixthInp"/>
                        </label>

                        <label className="SixthLabel">
                            <div className="SixthText">Номер телефону для зв’язку:</div>
                            <input type="text" value="+38 011 222 333 44" className="SixthInp"/>
                        </label>
                        <label className="SixthLabel">
                            <div className="SixthText">Адреса доставки:</div>
                            <input type="text" value="Область/Район/Населений пункт/Вулиця/Номер будинку/Номер квартири" className="SixthInp"/>
                        </label>
                        <label className="SixthGrey">

                            <input
                                name="isGoing"
                                type="checkbox"

                            />
                            Я відмовляюсь від БЕЗКОШТОВНОЇ доставки мені виграшу. Прошу вислати мені 317 000,00 гривень
                            поштовим переказом.
                        </label>
                        <label className="SixthBlack">

                            <input
                                name="isGoing"
                                type="checkbox"
                                checked="true"
                            />
                            Я заповнюю цей документ і роблю обов’язкове замовлення товарів.
                        </label>
                    </div>


                </form>

            </div>
        )
    }
}

export default Sixth
