import React, {Component} from 'react'
import down from "../images/cloud-sync.png"
import note from "../images/Group (1).png"
import finger from "../images/Vector (1).png"
import truck from "../images/truck.png"

class Fifth extends Component {
    render() {
        return (
            <div className="First">
                <div className="ThirdName"> ІНСТРУКЦІЯ З ОДЕРЖАННЯ ВИГРАШУ</div>
                <hr/>
                <div className="SecondRow">
                    <div className="App">
                        <img src={down} alt="" className="FifthImg"/>
                        <img src={note} alt="" className="FifthImg"/>
                        <img src={finger} alt="" className="FifthImg"/>
                    </div>
                    <div className="App">
                        <div className="FifthText">1. Завантажте ЧЕРВОНИЙ ДИПЛОМ ПЕРЕМОЖЦЯ або перепишіть його номер.
                            Ознайомтся з ДОДАТКОМ.
                        </div>
                        <div className="FifthText"> 2. Заповніть “БЛАНК НА ДОСТАВКУ ВИГРАШУ”. Обов’язково впишіть зручні
                            для Вас тиждень виплат та день одержання грошей, а також адресу.
                        </div>
                        <div className="FifthText">3. Оберіть уподобані товари з акційної товарної пропозиції. Акційна
                            пропозиція від PrizeMe дійсна лише протягом 24 годин! Замовте будь-які товари за СУПЕРЦІНОЮ!
                        </div>
                    </div>
                </div>
                <div className="FifthBord"> Увага: без Вашого замовлення “БЛАНК НА ДОСТАВКУ ВИГРАШУ” автоматично
                    відбракується Системою автоматичної верифікації, а отже, виплата Вам 317 000,00 гривень НЕ
                    ВІДБУДЕТЬСЯ!
                </div>
                <div className="SecondRow">
                    <img src={truck} alt="" className="FifthImg"/>
                    <div className="FifthText">4. Заберіть на пошті посилку із замовленими товарами та очікуйте кур’єра,
                        який БЕЗКОШТОВНО ПРИВЕЗЕ Вам гроші прямо додому.
                    </div>
                </div>
                <div className="FifthYel">Якщо НЕ НАДІШЛЕТЕ ВІДПОВІДЬ – ВТРАТИТЕ 317 000,00 гривень назавжди!</div>
                <div className="Empty"></div>
            </div>
        )
    }

}

export default Fifth
