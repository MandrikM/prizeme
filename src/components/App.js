import React, {Component} from 'react'
import Fifth from "./Fifth";
import Second from "./Second";
import Third from "./Third";
import First from "./First";
import Fourth from "./Fourth";
import Sixth from "./Sixth";
import img from "../images/Frame 5.3.jpg"

class App extends Component {
    render() {
        return (
            <div className="App">
                <First/>
                <Second/>
                <Third/>
                <Fourth/>
                <Fifth/>
                <Sixth/>
                <div className="SecondDip"> <button className="FourthButton"><a hhref="#selectProducts" >НАДІСЛАТИ</a></button></div>

                <div className="LastText">Щоб Служба автоматичної верифікації не відбракувала Ваш БЛАНК НА ДОСТАВКУ ВИГРАШУ, зробіть
                    обов’язкове замовлення товарів із акційної пропозиції.
                </div>
                <div id="selectProducts"><img src={img} alt=""/></div>
            </div>
        )
    }
}

export default App
