import React, {Component} from 'react'
import man from "../images/Depositphotos_5062302_xl-11 2.1.png"

class Third extends Component {
    render() {
        return (
            <div className="First">
                <div className="ThirdNameN">{"{" + "{" + "Name" + "}},"}</div>
                <div className="ThirdName">Ви – найкращий клієнт компанії!</div>
                <div className="ThirdYel">Ви – Власник ЧЕРВОНОГО ДИПЛОМА ПЕРЕМОЖЦЯ!</div>
                <hr/>
                <h3>Ми вже готові доставити Вам виграш за адресою Вашого проживання.
                    Головне – повідомте, коли Вам буде зручно зустріти кур’єра компанії та одержати гроші.
                </h3>
                <h3>Ви можете обрати будь-який тиждень виплат та день (навіть вихідний) для
                    візиту кур’єра.
                </h3>
                <img src={man} className="Img"/>
            </div>
        )
    }

}

export default Third
