import React, {Component} from 'react'
import logo from "../images/Group.png"
import girl from "../images/Depositphotos_27353059_xl-2015.png"
import podpis from "../images/filimonova-podpis (1).png"

class First extends Component {

    render() {
        return (
            <div className="First">

                <div className="First-first">*У разі Вашої перемоги в Акції "Золотий Фонд" та виникнення подібної ситуації Ви можете отримати такого листа</div>
                <img src={logo} className="FirstLogo" alt=""/>
                <h1>{"{" + "{" + "Name" + "}},"}</h1>
                <h2>На вас чекає грандіозне нагородження!</h2>
                <hr/>
                <h3> Від щирого серця вітаю Вас! Ви заслужено здобули перемогу і будете винагороджені по максимуму! Мені приємно усвідомлювати, що саме Ви отримаєте Головний приз Акції – 317 000,00 гривень!</h3>
                <button><a href="#myDiv">обрати дату нагородження</a></button>
                <img src={girl} className="Img" alt=""/>
                <div className="FirstDark">
                    <img src={podpis} alt=""/>
                    <div>З повагою, Голова Фінансового відділу з виплати призів Олена Філімонова.</div>
                </div>
            </div>
        )
    }
}

export default First
