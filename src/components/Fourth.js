import React, {Component} from 'react'
import frame from "../images/Group 4.png"

class Fourth extends Component {
    render() {
        return (
            <div id="myDiv" className="Second">
                <div className="FourthBig">Ми надсилаємо Вам “БЛАНК НА ДОСТАВКУ ВИГРАШУ”.</div>
                <img src={frame} className="FourthFrame" alt=""/>
                <div className="FourthName">{"{{" + "Name}}"}</div>
                <div className="FourthText">щоб отримати свій виграш – впишіть у “БЛАНК” зручну для Вас дату отримання
                    виграшу та надішліть нам. Після цього кур’єр “PrizeMe” доставить Вам 317 000,00 гривень прямо
                    додому.
                </div>
                <div className="FourthBord">ЗАПОВНЕНИЙ “БЛАНК НА ДОСТАВКУ ВИГРАШУ” – ВАША ГАРАНТІЯ ОТРИМАННЯ 317 000,00
                    ГРИВЕНЬ!
                </div>
                <div className="SecondDip"> <button className="FourthButton"><a href="#sixth" >ЗАПОВНИТИ БЛАНК</a></button></div>
            </div>
        )
    }
}

export default Fourth
