import React, {Component} from 'react'
import ImageZoom from 'react-medium-image-zoom'
import diploma from "../images/Frame 6.jpg"
import dod1 from "../images/Frame 5.2.jpg"
import dod2 from "../images/Frame 5.3.jpg"
import frame from "../images/Group 4.png"
import ImageScroller from "react-image-scroller";

class Second extends Component {
    constructor() {
        super();
        this.state = {
            showPopup: false
        };
    }

    togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
        });
    }

    render() {
        return (
            <div className="Second">
                <div className="SecondName">{"{{Name" + "}}, вітаємо!"}</div>
                <div className="SecondUsp">Ви успішно пройшли всі етапи відбору і отримуєте</div>
                <div className="SecondRed">ЧЕРВОНИЙ ДИПЛОМ ПЕРЕМОЖЦЯ!</div>
                <img src={frame} className="SecondFrame" alt=""/>
                <div className="SecondFin">Фінансовий відділ “PrizeMe” пишається Вами!</div>
                <div className="SecondRow">
                    <div>

                        <div className="SecondDiv">Ви дуже швидко реагували на листи і правильно заповнювали офіційні
                            документи, саме тому перед нагородженням ми вирішили видати Вам ЧЕРВОНИЙ ДИПЛОМ ПЕРЕМОЖЦЯ,
                            що надає Вам унікальне право на першочергову та БЕЗКОШТОВНУ доставку виграшу.
                        </div>
                        <div className="SecondDiv">Радійте! Гонитва за виграшем завершена. Ваші документи підписано,
                            гроші підготовлено. Тепер Ви гарантовано одержите 317 000,00 гривень без черг і на першу
                            вимогу.
                        </div>
                        <div className="SecondDiv">І зараз все, що лишилося – лише повідомити нам зручний для Вас день
                            доставки грошей! Це останній обов’язковий крок для отримання 317 000,00 гривень.
                        </div>
                        <div className="SecondDiv">Завантажте ДИПЛОМ або запишіть його номер для пред’явлення кур’єру
                            Служби доставки (за бажанням можете роздрукувати ДИПЛОМ, після завантаження). Ознайомтеся з
                            ДОДАТКОМ, щоб дізнатись усі подробиці Вашої перемоги.
                        </div>
                    </div>
                    <div className="SecondDip">
                       <div className="SecondDiv"> <ImageZoom
                            image={{
                                src: diploma,
                                style: {width: '100px'}
                            }}
                            zoomImage={{
                                src: diploma,
                                style: {width: '200px'}
                            }}
                       /></div>
                        <button className="SecondButton"><a href={diploma} download>Click to download</a></button>
                        <button onClick={this.togglePopup.bind(this)} className="SecondDod"><img src={dod2} style={{width: '100px'}}/>
                        </button>
                    </div>
                </div>


                <div className="SecondRed">Завдяки Вашому ЧЕРВОНОМУ ДИПЛОМУ ПЕРЕМОЖЦЯ доставку виграшу буде здійснено
                    БЕЗКОШТОВНО!
                </div>
                {this.state.showPopup ?
                    <Popup
                        text='Close Me'
                        closePopup={this.togglePopup.bind(this)}
                    />
                    : null
                }

            </div>
        )
    }

}


export default Second

class Popup extends Component {
    render() {
        return (
            <div>
                <div>
                    <ImageScroller>
                        <img src={dod1} alt="First"/>
                        <img src={dod2} alt="Second"/>

                    </ImageScroller>
                    <button onClick={this.props.closePopup}>close me</button>
                </div>
            </div>
        );
    }
}
